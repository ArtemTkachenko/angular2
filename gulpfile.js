var gulp = require('gulp');
var connect = require('gulp-connect');


gulp.task('connectDist', function () {
    connect.server({
        root: '',
        port: 9999,
        livereload: true
    });
});

gulp.task('default', ['connectDist']);